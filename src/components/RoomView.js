import React from 'react';
import { View, FlatList, StyleSheet, Text, TouchableOpacity } from 'react-native';

const RoomView = ({ data }) => {
    const name = Object.values(data).map((r, i) => <Text key={i} style={styles.title}>{r.name}</Text>);
    return (
        <View style={styles.container}>
            <Text style={styles.title_box}>Topic Of The Day</Text>
            {name}
            <Text style={styles.description}>Click to Join</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
    },
    title_box: {
        paddingTop: 5,
        paddingLeft: 10,
    },
    title: {
        fontSize: 18,
        color: '#000',
        marginVertical: 5,
        paddingLeft: 10,
    },
    description: {
        fontSize: 11,
        fontStyle: 'italic',
        textAlign: 'right',
        paddingRight: 10,
        paddingTop: 10,
    },
});

export default RoomView;