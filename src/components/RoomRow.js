import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        padding: 5,
        marginLeft: 16,
        marginRight: 16,
        marginTop: 30,
        // marginBottom: 8,
        borderRadius: 5,
        backgroundColor: '#FFF',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    title: {
        fontSize: 16,
        color: '#000',
    },
    container_text: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 12,
        justifyContent: 'center',
    },
    description: {
        fontSize: 11,
        fontStyle: 'italic',
    },
});

const RoomRow = ({ item, navigation }) => {
    return (
        <TouchableOpacity onPress={() => navigation.navigate('ConferenceRoom', { data: item })}>
            <View style={styles.container}>
                <View style={styles.container_text}>
                    <Text style={styles.title}>
                        {item.name}
                    </Text>
                    <Text style={styles.description}>Click to Join</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
};

export default RoomRow;