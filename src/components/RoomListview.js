import React from 'react';
import { View, FlatList, StyleSheet, Text } from 'react-native';
import RoomRow from './RoomRow';

const RoomListview = ({ itemList, navigation }) => {
    return (
        <View style={styles.container}>
            <FlatList
                data={itemList}
                renderItem={({ item, index }) => <RoomRow key={index} item={item} navigation={navigation}/>}
            />

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default RoomListview;