import { createStackNavigator, createAppContainer } from "react-navigation";
import { fromRight } from "react-navigation-transitions";

import Login from './screens/Login';
import HomeScreen from './screens/HomeScreen';
import ConferenceRoom from './screens/ConferenceRoom';

const AppNavigator = createStackNavigator(
    {
        Login: {
            screen: Login,
            navigationOptions: {
                header: null
            }
        },
        Home: {
            screen: HomeScreen,
            navigationOptions: {
                header: null
            }
        },
        ConferenceRoom: {
            screen: ConferenceRoom,
        },
    },
    {
        initialRouteName: "Home",
        transitionConfig: () => fromRight(1000)
    }
);

export default createAppContainer(AppNavigator);