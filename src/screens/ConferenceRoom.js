import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button } from 'react-native';
import { OTSession, OTPublisher, OTSubscriber } from 'opentok-react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import SoundPlayer from 'react-native-sound-player'

export default class ConferenceRoom extends Component {
    constructor(props) {
        super(props);
        this.state = {
            apiKey: '',
            // time: (10000 / 1000),
            time: undefined,
            stopTime: true,
            speak: false
        };
        this.timerButton;
        this.timerOption;
        this.cancelTimerButton = false;

        this.sessionEventHandlers = {
            signal: (event) => {
                const { speak, time } = JSON.parse(event.data)
                if (speak && time > 0) this.setState({ speak: true, time }, () => this.setTimer())
            }
        }

        this.publisherProperties = {
            publishVideo: false,
            publishAudio: true,
            cameraPosition: 'front'
        };

        this.subscriberProperties = {
            subscribeToAudio: true,
            subscribeToVideo: false,
          };
    }

    getSound = () => {
        try {
            // SoundPlayer.playUrl('http://www.music.helsinki.fi/tmt/opetus/uusmedia/esim/a2002011001-e02-128k.mp3')
            SoundPlayer.playSoundFile('a_tone', 'mp3')
        } catch (e) {
            console.log(`cannot play the sound file`, e)
        }
    }

    setTimer = () => {
        clearTimeout(this.timerButton);
        this.timerButton = setTimeout(() => {
            let { time } = this.state;
            if (!this.cancelTimerButton) {
                if (time > 0) {
                    time--;
                    this.setState({ time }, () => {
                        if (this.state.time == 5) this.getSound();
                    })
                } else {
                    clearTimeout(this.timerButton);
                    this.setState({ speak: false })
                    this.cancelTimerButton = true
                }
                this.setTimer();
            }
        }, 1000);

    }


    render() {
        let data = Object.values(this.props.navigation.state.params.data).map(r => r)
        const { time, speak } = this.state;
        return (
            <View style={styles.container}>
                <Text>{time}</Text>
                <OTSession apiKey={data[0].apiKey} sessionId={data[0].sessionId} token={data[0].token} eventHandlers={this.sessionEventHandlers}>
                    {/* self */}
                    <OTPublisher style={{ width: 200, height: 200 }} properties={this.publisherProperties} />
                    {/* customer */}
                    <OTSubscriber style={{ width: 200, height: 200 }} properties={this.subscriberProperties}/>
                </OTSession>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
