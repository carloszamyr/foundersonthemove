import React from 'react'
import { StyleSheet, View } from 'react-native'

import LinkedInModal from 'react-native-linkedin'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'gray',
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default class Login extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <LinkedInModal
          clientID="7725h2s7bj0jb0"
          clientSecret="OaLq69EZDHr77RRQ"
          redirectUri="https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=7725h2s7bj0jb0&redirect_uri=https%3A%2F%2Fdev.example.com%2Fauth%2Flinkedin%2Fcallback&state=fooobar&scope=r_liteprofile%20r_emailaddress%20w_member_social"
          onSuccess={token => console.warn(token)}
        />
      </View>
    )
  }
}