import React, { Component } from 'react';
import { View, Platform, StyleSheet, TouchableOpacity, Text } from 'react-native';
import axios from 'axios';
import RoomView from '../components/RoomView';

class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rooms: [],
            coordinates: null
        }
    }

    componentDidMount() {
        this.getPosition()

    }

    getPosition = () => {
        navigator.geolocation.getCurrentPosition(
            position => {
                const coordinates = position.coords
                this.getData(coordinates);
            }, error => console.log(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
    }

    getData = async (coordinates) => {
        this.setState({ coordinates });
        const rooms = await axios.get('https://api-key-generator.herokuapp.com/chatrooms')
        let place = '';
        let minDistance = 0;
        let apiKey = '';
        let sessionId = '';
        let token = '';
        // console.log(rooms.data)
        if (coordinates !== null) {
            let Ownlat = coordinates.latitude;
            rooms.data.map(r => {
                let difLat = Math.abs(Ownlat - parseFloat(r.lat))
                let difLatKm = (difLat * 110.574) / 1;
                let difLonKm = (111.32 * Math.cos(difLat))
                let distancies = Math.sqrt(Math.pow(difLatKm, 2) + Math.pow(difLonKm, 2))
                if (minDistance == 0) {
                    minDistance = distancies;
                    place = r.name;
                    apiKey = r.apiKey;
                    sessionId = r.sessionId;
                    token = r.token;
                } else if (distancies < minDistance) {
                    minDistance = distancies;
                    place = r.name;
                    apiKey = r.apiKey;
                    sessionId = r.sessionId;
                    token = r.token;
                }
                // console.log('KM ', Ownlat, parseFloat(r.lat), r.lon, r.name + ' - ' + distancies);
            });
        }

        this.setState({ rooms: [{ name: place, distance: minDistance, apiKey, sessionId, token }] });
    }

    render() {
        const item = Object.values(this.state.rooms).map(r => r);
        return (
            <View style={styles.container}>
                <Text style={styles.title_box}>Founder On The Move</Text>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('ConferenceRoom', { data: item })}>
                    <View style={styles.shadowBox}>
                        <RoomView data={this.state.rooms} />
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    title_box: {
        fontSize: 18,
        marginTop: 40,
        textAlign: 'center'
    },
    texto: {
        fontSize: 20,
        textAlign: "center",
        margin: 10
    },
    shadowBox: {
        padding: 5,
        marginLeft: 16,
        marginRight: 16,
        marginTop: 20,
        height: 100,
        borderRadius: 5,
        backgroundColor: '#FFF',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    }
});

export default HomeScreen;